from django.contrib.auth.forms import UserCreationForm
from .models import CustomUser
from django.contrib.auth import login, authenticate
from . mixins import RequestKwargModelFormMixin
from .models import Cart
# from .views import get_user_pin


class SingUpForm(RequestKwargModelFormMixin, UserCreationForm):

                # def save(self):
                # 	print("upper")
                # return("suf")

    class Meta:
        model = CustomUser
        fields = ["first_name", "last_name", "username"]

    def _get_user_pin(self):
        '''
        output request user pin
        '''
        if self.request.user.is_authenticated:
            pin = self.request.user.pin
        else:
            pin = self.request.session.get("pin")
        return pin

    def save(self, commit=True):

        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.pin = self._get_user_pin()
            user.save()
            user_cart = Cart.objects.get_or_create_cart(self.request)
            # auth_user = authenticate(self.request, username=self.cleaned_data["username"],
            #                          password=self.cleaned_data["password1"])
            user_cart.owner = user
            login(self.request, user)
            user_cart.save()
        return user

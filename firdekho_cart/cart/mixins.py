from django.views.generic.base import TemplateResponseMixin


class RequestFormKwargsMixin(object):

    # """
    # CBV mixin which puts the request into the form kwargs.
    # Note: Using this mixin requires you to pop the `request` kwarg
    # out of the dict in the super of your form's `__init__`.
    # """

    def get_form_kwargs(self):
        kwargs = super(RequestFormKwargsMixin, self).get_form_kwargs()
        # Update the existing form kwargs dict with the request's user.
        kwargs.update({"request": self.request})
        return kwargs


class RequestKwargModelFormMixin(object):

    # """
    # Generic model form mixin for popping request out of the kwargs and
    # attaching it to the instance.

    # This mixin must precede forms.ModelForm/forms.Form. The form is not
    # expecting these kwargs to be passed in, so they must be popped off before
    # anything else is done.
    # """

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)  # Pop the request off the passed in kwargs.
        super(RequestKwargModelFormMixin, self).__init__(*args, **kwargs)


# class CustomTemplateResponseMixin(TemplateResponseMixin):

#     def get_template_names(self):
#         """
#         Return a list of template names to be used for the request. Must return
#         a list. May not be called if render_to_response() is overridden.
#         """
#         if self.template_name is None:
#             raise ImproperlyConfigured(
#                 "TemplateResponseMixin requires either a definition of "
#                 "'template_name' or an implementation of 'get_template_names()'")
#         elif request.user.user_type == "BR":
#             return ["buyerdetails.html"]
#         else:
#             return [self.template_name]

from django.db.models import Manager


class CartModelManager(Manager):

    def get_or_create_cart(self, request):
        if request.user.is_authenticated:
            cart, created = self.model.objects.get_or_create(owner=request.user)
            return cart
        else:
            cart_id = request.session.get("cart_id", None)
            cart, created = self.model.objects.get_or_create(id=cart_id)
            request.session["cart_id"] = cart.id
            return cart
        # print(cart_id)
        # if cart_id is not None:
        #     return self.get_queryset().get(id=cart_id)
        # else:
        #     cart = self.model.objects.create(owner=request.user)
        #     print("creation cart")
        #     request.session["cart_id"] = cart.id
        #     return cart

from django.apps import AppConfig
from django.db.models.signals import post_migrate
from .views import create_init_db


class CartConfig(AppConfig):
    name = 'cart'

    def ready(self):
        print("working")
        post_migrate.connect(create_init_db, sender=self)

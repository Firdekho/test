from django.db import models
from django.contrib.auth.models import User
from .manager import CartModelManager
from django.contrib.auth.models import AbstractUser
# from order.models import Order

# Create your models here.


class CustomUser(AbstractUser):
    state = models.CharField(max_length=240, null=True)
    city = models.CharField(max_length=240, null=True)
    pin = models.IntegerField(default=0, blank=False, null=True)
    seller = models.BooleanField(default=False)
    default_address = models.OneToOneField('order.Address',
                                           on_delete=models.CASCADE,
                                           related_name='defalut_add',
                                           null=True)

    def __str__(self):
        return self.username


class Product(models.Model):

    name = models.CharField(max_length=240)
    price = models.IntegerField(default=0, null=False)
    seller = models.ForeignKey(CustomUser, related_name="seller_pro",
                               on_delete=models.CASCADE, null=True)
    # order = models.ForeignKey('order.Order', on_delete=models.DO_NOTHING,
    #                           related_name="order_pro", null=True)

    def __str__(self):
        return self.name


class Cart(models.Model):

    owner = models.OneToOneField(CustomUser, null=True,
                                 blank=False, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product)
    total_value = models.IntegerField(default=0, blank=True)
    total_item = models.IntegerField(default=0, null=True)
    shipping_local = models.IntegerField(default=0, null=True)
    shipping_regional = models.IntegerField(default=0, null=True)
    shipping_national = models.IntegerField(default=0, null=True)

    objects = CartModelManager()

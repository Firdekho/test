from django.shortcuts import render
from django.views.generic import ListView
from . models import Product, CustomUser
from django.shortcuts import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Cart
from django.contrib.auth import get_user_model
from django.db.models.signals import m2m_changed
from django.core.signals import request_started
from django.dispatch import receiver
from functools import reduce
from rest_framework.generics import UpdateAPIView
from django.conf import settings
import os
import pandas as pd
from random import randint
from django.db.models.signals import post_migrate
from django.contrib.auth.views import LoginView
from django.views.generic import TemplateView
from django.urls import reverse_lazy, reverse
from .forms import SingUpForm
from django.views.generic.edit import CreateView
from django.views.generic.base import RedirectView
from . mixins import RequestFormKwargsMixin


class StoreView(ListView):
    template_name = 'index.html'
    queryset = Product.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user_cart = Cart.objects.get_or_create_cart(self.request)
        if user_cart:
            context['user_cart_products'] = user_cart.products.all()
            context.update(kwargs)
            return context
        else:
            return context


def test(request):
    cart = Cart.objects.get_or_create_cart(request)
    print(request.user)
    user_cart = Cart.objects.get_or_create_cart(request)
    print(user_cart)
    print(request.session.session_key)
    print(request.session.get("pin"))
    print(request.session.get("cart_id"))
    print(cart)
    return HttpResponse("i")


def get_cart(request):
    if request.user.is_authenticated:
        return Cart.objects.get_or_create_cart(request)
    else:
        return Cart.objects.get_or_create_cart(request)


@api_view(['POST'])
def create_or_add_to_cart(request):
    cart = Cart.objects.get_or_create_cart(request)
    product = Product.objects.get(name=request.data["product"])
    cart.products.add(product)
    return Response("added")


@api_view(['POST'])
def remove_from_cart(request):
    cart = Cart.objects.get_or_create_cart(request)
    product = Product.objects.get(name=request.data["product"])
    cart.products.remove
    (product)
    return Response("remove")


@receiver(m2m_changed, sender=Cart.products.through)
def cart_value_update(sender, instance, action, **kwargs):
    if action == "post_add" or action == "post_remove":
        instance.total_value = reduce(lambda price1, price2: price1 + price2,
                                      [pro.price for pro in instance.products.all()])
        instance.save()


def read_csv():
    '''
    read and modif csv return dataframe
    '''
    pincodes = os.path.join(settings.PROJECT_ROOT, 'pincodes.csv')
    df = pd.read_csv(pincodes)
    df['Pincode'] = df['Pincode'].fillna(0)
    return df


class CartView(ListView):
    template_name = 'cart.html'

    def get_queryset(self):
        cart = Cart.objects.get_or_create_cart(self.request)
        return cart.products.all()

    def _get_location(self, pin, df):
        '''
        Input product pin and DataFram.
        output Seller state & City
        '''
        loc = df.loc[df["Pincode"] == pin].iloc[0]
        return loc["State"], loc["City"]

    def _get_user_pin(self):
        '''
        output request user pin
        '''
        if self.request.user.is_authenticated:
            pin = self.request.user.pin
        else:
            pin = self.request.session.get("pin")
        return pin

    def _product_type(self, cart):
        '''
        output three list item. local contain products from same city.
        regional contain product from product from same state.
        national contain product out of the state
        '''
        local = []
        regional = []
        national = []
        df = read_csv()
        for f in cart.products.all():
            seller_state, seller_city = self._get_location(f.seller.pin, df)
            user_state, user_city = self._get_location(self._get_user_pin(), df)
            if user_city == seller_city:
                local.append(f)
                cart.shipping_local = 30
            elif user_state == seller_state:
                regional.append(f)
                cart.shipping_regional = 40
            else:
                national.append(f)
                cart.shipping_national = 50
            cart.save()
        return local, regional, national

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart = Cart.objects.get_or_create_cart(self.request)

        try:
            '''
            try catch can be remove if dataframecon if DataFram
            Pincode column doesn't contain NAN or 0
            '''
            self._product_type(cart)

        except IndexError:
            context['pin_error'] = True
        context['cart'] = cart
        context["pin"] = self._get_user_pin()
        context.update(kwargs)
        return context


@api_view(['POST'])
def update_pin(request):
    if request.user.is_authenticated:
        request.user.pin = int(request.data["pin"])
        request.user.save()
    else:
        request.session["pin"] = int(request.data["pin"])
    return Response("updated")


def create_seller():
    df = read_csv()
    row = df.iloc[randint(0, len(df.index))]
    for i in range(2):
        seller, created = CustomUser.objects.get_or_create(username="seller" + str(randint(1, 2000)),
                                                           first_name="sellername" + str(i),
                                                           seller=True, city=row["City"],
                                                           pin=row["Pincode"],
                                                           state=row["State"])

        product, created = Product.objects.get_or_create(name="product" + str(randint(1, 2000)),
                                                         price=randint(200, 5000),
                                                         seller=seller)


# @receiver(post_migrate, sender=self)
# @receiver(request_started)
def create_init_db(sender, **kwars):
    ''' 
    reciver function 
    populate database with 2 seller and 2 product of each seller
    '''
    create_seller()


class OrderprocedeView(LoginView):

    template_name = 'order_procede.html'


class CheckoutView(ListView):
    template_name = "checkout.html"

    def get_queryset(self):
        return Product.objects.all()


class SingUpView(CreateView):
    '''
    main singup View
    '''
    template_name = 'usercreation.html'
    form_class = SingUpForm
    success_url = reverse_lazy('cart:store')


class CheckoutSingUpView(RequestFormKwargsMixin, SingUpView):
    '''
    checkout singup view
    add the existing cart to created user.

    '''
    success_url = reverse_lazy('order:address')


class CheckOutSinginView(LoginView):
    template_name = "login.html"
    # success_url = reverse_lazy('cart:store')

    def get_success_url(self):
        url = self.get_redirect_url()
        return url or reverse_lazy('order:address')


class OrderRedirectView(RedirectView):
    '''
    redirect Authticated user to chackeout page.
    redirect Anonymous user to singup page

    '''

    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            url = reverse('order:address')
        else:
            url = reverse('cart:checkout_singup')
        return url

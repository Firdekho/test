# Generated by Django 2.1.5 on 2019-02-09 11:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0015_auto_20190206_2031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='seller',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='seller_pro', to=settings.AUTH_USER_MODEL),
        ),
    ]

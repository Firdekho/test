from .models import CustomUser, Product
from . views import read_csv
from random import randint
django.core.signals import request_started
from django.dispatch import receiver

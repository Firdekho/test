from django.urls import path
from .views import (StoreView, CartView, OrderprocedeView,
                    CheckoutView, OrderRedirectView, CheckOutSinginView, CheckoutSingUpView)
from. import views
from django.contrib.auth.views import LoginView

app_name = "cart"


urlpatterns = [
    path('', StoreView.as_view(), name='store'),
    path('1/', views.test),
    path('create_or_add_to_cart/', views.create_or_add_to_cart, name="create_or_add_to_cart"),
    path('cart/', CartView.as_view(), name="cart"),
    path('updatepin/', views.update_pin, name="updatepin"),
    path('orderredirect/', OrderRedirectView.as_view(), name='order_redirect'),
    path('order_procede/', OrderprocedeView.as_view(), name="order_procede"),
    path('checkout/', CheckoutView.as_view(), name="checkout"),
    path('checkoutsingup/', CheckoutSingUpView.as_view(), name="checkout_singup"),
    path('checkoutlogin/', CheckOutSinginView.as_view(), name="checkout_singin"),


]

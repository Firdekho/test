from django.core.exceptions import ValidationError
# from django.conf import settings
# import os
# import pandas as pd
from django import forms
from . models import Address, Order
from cart.mixins import RequestKwargModelFormMixin
from cart.models import Cart
from cart.views import read_csv


class AddressForm(RequestKwargModelFormMixin, forms.ModelForm):

    class Meta:
        model = Address
        exclude = ["user"]

    def save(self):
        instance = super().save(commit=True)
        instance.user = (self.request.user)
        instance.save()
        self.request.user.default_address = instance
        self.request.user.save()
        return instance


class PaymentForm(forms.Form):

    payment_mthd = (("COD", "Cash On delivery"), ("O", "Others"))
    payment_method = forms.ChoiceField(choices=payment_mthd,
                                       initial="COD", required=True,
                                       widget=forms.RadioSelect)

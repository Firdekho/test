from django.urls import path
from .views import (OrderAddress, CheckOut)
from . import views

app_name = "order"


urlpatterns = [
    path('address/', OrderAddress.as_view(), name='address'),
    path('checkout/', CheckOut.as_view(), name="checkout"),
    path('setuserdefaultaddress/', views.set_default_address_update, name='set_default_address_update'),
    path('summary/', views.orders_summary, name='summary')
]

from django.views.generic.edit import CreateView
from django.core.exceptions import ValidationError
from .forms import AddressForm, PaymentForm
from django.views.generic import TemplateView, ListView, FormView
from django.urls import reverse_lazy
from cart.mixins import RequestFormKwargsMixin
from rest_framework.generics import UpdateAPIView
from cart.serializers import CustomUserSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Address, Order
from django.shortcuts import render
from cart.models import Cart
from .mixins import LocationMixins
from cart.views import read_csv
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required


class OrderAddress(RequestFormKwargsMixin, CreateView):

    template_name = 'order_address.html'
    form_class = AddressForm
    success_url = reverse_lazy('order:checkout')
    # extra_context = (self.request.user.address_set.all())

    def get_context_data(self, **kwargs):
        kwargs["user_address_list"] = self.request.user.address_set.all()
        return super().get_context_data(**kwargs)

        # def form_valid(self, form):
        #     object = super().form_valid(form)
        #     object.user(self.request.user)
        #     return object


class UserDefaultAddressUpdate(LoginRequiredMixin, UpdateAPIView):

    serializer_class = CustomUserSerializer

    def get_queryset(self):
        queryset = self.request.user
        return queryset


@api_view(["POST"])
def set_default_address_update(request):
    '''
    set user default delivery address
    '''
    address_id = int(request.data.get("address_id"))
    # user = request.user
    address = Address.objects.get(id=address_id)
    request.user.default_address = address
    request.user.save()
    return Response("success")


# class BaseCheckOut(LocationMixins, CreateView):
#     "Base view for Checkout"


class CheckOut(LocationMixins, FormView):
    template_name = 'order_checkout.html'
    form_class = PaymentForm
    # success_url = reverse_lazy('order:summary')

    def get_success_url(self):
        if self.request.POST.get("payment_method") == "COD":
            return reverse_lazy("payment:cod")

    def get_queryset(self):
        cart = Cart.objects.get_or_create_cart(self.request)
        return cart.products.all()

    def _shippingcharges(self, cart, context):
        '''
        output three list item. local contain products from same city.
        regional contain product from product from same state.
        national contain product out of the state
        '''

        df = read_csv()
        for f in cart.products.all():
            seller_state, seller_city = self.get_location(f.seller.pin, df)
            delivery_state, delivery_city = self.get_location(self.get_delivery_pin(), df)
            context["total"] = 0
            if delivery_city == seller_city:
                context['local'] = int(30)
                context["total"] = sum([context.get("total"), context.get("local")])
            elif delivery_state == seller_state:
                context['regional'] = int(40)
                context["total"] = sum([context.get("total"), context.get("regional")])
            else:
                context['national'] = int(50)
                context["total"] = sum([context.get("total"), context.get("national")])
        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart = Cart.objects.get_or_create_cart(self.request)
        print(cart)
        context["order_value"] = cart.total_value

        try:
            '''
            try catch can be remove if dataframecon if DataFram
            Pincode column doesn't contain NAN or 0
            '''
            self._shippingcharges(cart, context)

        except Exception as e:
            print(e)
        context.update(kwargs)
        return context


def orders_summary(request):

    act_order = Order.objects.filter(user=request.user)
    context = {"orders": act_order}
    return render(request, 'order_summary.html', context)

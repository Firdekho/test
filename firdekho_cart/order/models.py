from django.db import models
from .validator import pin_validatot
# from cart.models import CustomUser
# Create your models here.


class Address(models.Model):
  user = models.ForeignKey('cart.CustomUser', on_delete=models.CASCADE,
                           null=True)
  address = models.CharField(max_length=500)
  state = models.CharField(max_length=120)
  city = models.CharField(max_length=120)
  pin = models.PositiveIntegerField(validators=[pin_validatot])


class Order(models.Model):
  user = models.ForeignKey('cart.CustomUser', on_delete=models.CASCADE,
                           null=True)
  address = models.ForeignKey(Address, on_delete=models.DO_NOTHING, null=True)
  order_value = models.PositiveIntegerField(default=0)
  shipping = models.PositiveIntegerField(default=0)
  order_discoutn = models.PositiveIntegerField(default=0)
  activate = models.BooleanField(default=True)
  cancel = models.BooleanField(default=False)
  created = models.DateTimeField(auto_now_add=True)
  delivery_time = models.PositiveIntegerField(default=7, null=True)
  products = models.ManyToManyField('cart.Product', related_name="odr_pro")

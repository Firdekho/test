from django.conf import settings
import os
import pandas as pd
from django.core.exceptions import ValidationError


def pin_list():
    '''
    return list of all pincode
    '''
    pincodes = os.path.join(settings.PROJECT_ROOT, 'pincodes.csv')
    df = pd.read_csv(pincodes)
    # df['Pincode'] = df['Pincode'].fillna(0)
    return df['Pincode'].fillna(0).values.tolist()


def pin_validatot(value):
    '''
    Address From Field Validator .

    '''
    if value not in pin_list():
        raise ValidationError("Enter correct pin")

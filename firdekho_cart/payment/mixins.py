class LocationMixins:

    def get_location(self, pin, df):
        '''
        Input product pin and DataFram.
        output Seller state & City
        '''
        print("get_location")
        loc = df.loc[df["Pincode"] == pin].iloc[0]
        return loc["State"], loc["City"]

    def get_delivery_pin(self):
        '''
         output request user pin
         '''
        return self.request.user.default_address.pin

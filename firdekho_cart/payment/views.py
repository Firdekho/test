from django.shortcuts import render
from django.urls import reverse_lazy
from cart.models import Cart
from .mixins import LocationMixins
from django.views.generic import RedirectView
from cart.mixins import RequestFormKwargsMixin
from cart.views import read_csv
# from .forms import PaymentForm
from django.contrib.auth.mixins import LoginRequiredMixin
from order.models import Order
from .models import CashOnDeliveryTransaction
from django.conf import settings
import requests
import json
# Create your views here.


class BaseCodView(LocationMixins, RedirectView):
    "Base view for CodView"


class CodView(LoginRequiredMixin, BaseCodView):
    template_name = 'order_checkout.html'
    # form_class = PaymentForm
    url = reverse_lazy('order:summary')

    def get_queryset(self):
        cart = Cart.objects.get_or_create_cart(self.request)
        return cart.products.all()

    def _shippingcharges(self, cart, context):
        '''
        output three list item. local contain products from same city.
        regional contain product from product from same state.
        national contain product out of the state
        '''

        df = read_csv()
        for f in cart.products.all():
            seller_state, seller_city = self.get_location(f.seller.pin, df)
            delivery_state, delivery_city = self.get_location(self.get_delivery_pin(), df)
            context["total"] = 0
            if delivery_city == seller_city:
                context['local'] = int(30)
                context["total"] = sum([context.get("total"),
                                        context.get("local")])
            elif delivery_state == seller_state:
                context['regional'] = int(40)
                context["total"] = sum([context.get("total"),
                                        context.get("regional")])
            else:
                context['national'] = int(50)
                context["total"] = sum([context.get("total"),
                                        context.get("national")])
        return context

    def _create_order(self, cart):
        '''
        create order.
        relate all product from cart to order.
        update total shipping price
        '''
        order, created = Order.objects.get_or_create(
            user=self.request.user,
            order_value=cart.total_value)
        order.save()
        df = read_csv()
        local_shipping = 0
        regional_shipping = 0
        national_shipping = 0
        for f in cart.products.all():
            seller_state, seller_city = self.get_location(f.seller.pin, df)
            delivery_state, delivery_city = self.get_location(self.get_delivery_pin(), df)
            if delivery_city == seller_city:
                local_shipping = 30
            elif delivery_state == seller_state:
                regional_shipping = 40
            else:
                national_shipping = 50
            order.products.add(f)
        order.shipping = sum([local_shipping,
                              regional_shipping,
                              national_shipping])
        order.address = self.request.user.default_address
        order.save()
        return order

    def _create_cod(self, order):
        cod, created = CashOnDeliveryTransaction.objects.get_or_create(order=order,
                                                                       amount=sum([order.order_value, order.shipping]),
                                                                       confirmed=True)
        cod.save()
        return cod

    def _request_payload(self):
        data = {
            "shipments": [
                {
                    "tax_value": "taxvalue",
                    "consignee_gst_amount": "for ewaybill-incase of intra-state required only",
                    "seller_tin": "sellertin",
                    "integrated_gst_amount": "for ewaybill-incase of intra-state required only",
                    "ewbn": "if ewbn is there no need to send additional keys for generating ewaybill",
                    "seller_gst_amount": "for ewaybill-incase of intra-state required only",
                    "seller_inv": "sellerinv",
                    "city": "Kota",
                    "commodity_value": "commodityvalue",
                    "weight": "weight(gms)",
                    "return_state": "returnstate",
                    "document_number": "for ewaybill-document_number",
                    "add": "M25,NelsonMarg",
                    "od_distance": "ditance between origin and destination",
                    "address_type": "Home or Office",
                    "consignee_gst_tin": "consignee_gst_tin",
                    "sales_tax_form_ack_no": "ackno.",
                    "phone": 1234567890,
                    "payment_mode": "Prepaid/COD/Pickup/REPL",
                    "document_type": "for ewaybill-document_type",
                    "seller_gst_tin": "seller_gst_tin",
                    "seller_cst": "sellercst",
                    "name": "nameofconsignee",
                    "seller_name": "sellername",
                    "return_city": "returncity",
                    "return_phone": "returnphone",
                    "client_gst_tin": "client_gst_tin",
                    "qc": {
                        "item": [
                            {
                                "color": "Color of the product",
                                "reason": "Damaged Product/Return reason of the product",
                                "images": "image Images of the product to be picked. Need to give Image URL with image size not more than 80KB. Maximum image count: 2",
                                "descr": "description of the product",
                                "ean": "EAN no. that needs to be checked for a product (apparels)",
                                "imei": "IMEI no. that needs to be checked for a product (mobile phones)",
                                "brand": "Brand of the product",
                                "pcat": "Product category like mobile, apparels etc.",
                                "si": "special instruction for FE"
                            }
                        ]
                    },
                    "product_quantity": "for ewaybill-mandatory,incase of intra-state required only",
                    "category_of_goods": "categoryofgoods",
                    "cod_amount": 2125,
                    "return_country": "returncountry",
                    "shipment_width": "shipmentwidth",
                    "pin": 325007,
                    "document_date": "for ewaybill-datetime",
                    "taxable_amount": "for ewaybill-taxable_amount in case of multiple items only",
                    "products_desc": "for ewaybill-mandatory,incase of intra-state required only",
                    "hsn_code": "Required for ewaybill-hsn_code",
                    "state": "Rajasthan",
                    "gst_cess_amount": "for ewaybill-gst_cess_amount",
                    "waybill": "waybillno.(trackingid)",
                    "consignee_tin": "consigneetin",
                    "order_date": "2017-05-20 12:00:00",
                    "return_add": "returnaddress",
                    "total_amount": 21840,
                    "seller_add": "selleradd",
                    "country": "India",
                    "return_pin": "returnpin",
                    "extra_parameters": {
                        "return_reason": "string"
                    },
                    "client": "clientnameasregistered",
                    "return_name": "name",
                    "order": "orderid",
                    "supply_sub_type": "for ewaybill-supply_sub_type",
                    "quantity": "quantity"
                }
            ],
            "pickup_location": [
                {
                    "city": "city",
                    "name": "UseClientWarehousename",
                    "pin": "pin-code",
                    "country": "country",
                    "phone": "phoneno.",
                    "add": "addressofwarehouse"
                }
            ]
        }

        return data

    def _request_header(self):
        headers = {
            'Accept: application/json',
            'Authorization: bf4662dbb5c8a7a96a8b4d4c444cf8b03fa19622'
        }

        return headers

    def _create_test_package(self, url):
        headers = self._request_header()
        data = json.dumps(self._request_payload())
        res = requests.post(url, data=data, headers=headers)
        res_json = res.json()
        return res_json

    def get(self, request, *args, **kwargs):

        cart = Cart.objects.get_or_create_cart(self.request)

        if settings.DEBUG:
            try:
                url = 'http://test.delhivery.com/api/cmu/create.json'
                res = self._create_test_package(url)
            except:
                res = {

                    "cash_pickups_count": "string",
                    "package_count": "string",
                    "upload_wbn": "string",
                    "replacement_count": "string",
                    "pickups_count": "string",
                    "packages": [
                        {
                            "status": "string",
                            "client": "string",
                            "waybill": "string",
                            "remarks": [
                                {
                                    "": "string"
                                }
                            ],
                            "sort_code": "string",
                            "cod_amount": "string",
                            "payment": "string",
                            "serviceable": "string",
                            "refnum": "string"
                        }
                    ],
                    "cash_pickups": "string",
                    "cod_count": "string",
                    "success": "string",
                    "prepaid_count": "string",
                    "cod_amount": "string"
                }

                # try:
                #     '''
                #     try catch can be remove if dataframecon if DataFram
                #     Pincode column doesn't contain NAN or 0
                #     '''
                #     order = self._create_order(cart)
                #     self._create_cod(order)

        order = self._create_order(cart)
        self._create_cod(order)
        cart.delete()
        return super().get(request, *args, **kwargs)

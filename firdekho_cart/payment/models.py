from django.db import models
import uuid
from django.utils.translation import ugettext_lazy as _
# Create your models here.


def _make_uuid():

    return str(uuid.uuid4())


class CashOnDeliveryTransaction(models.Model):
    order = models.OneToOneField('order.Order', on_delete=models.CASCADE, blank=True,
                                 null=True)
    date_created = models.DateTimeField(_('Date Created'), auto_now_add=True)
    amount = models.PositiveIntegerField(null=True, blank=True)
    reference = models.CharField(max_length=100, blank=True, unique=True, default=_make_uuid)
    confirmed = models.BooleanField(default=False)
    date_confirmed = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_created',)

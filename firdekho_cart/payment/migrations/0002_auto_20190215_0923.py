# Generated by Django 2.1.5 on 2019-02-15 09:23

from django.db import migrations, models
import django.db.models.deletion
import payment.models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0009_auto_20190211_1556'),
        ('payment', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cashondeliverytransaction',
            name='currency',
        ),
        migrations.RemoveField(
            model_name='cashondeliverytransaction',
            name='order_number',
        ),
        migrations.AddField(
            model_name='cashondeliverytransaction',
            name='order',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='order.Order'),
        ),
        migrations.AlterField(
            model_name='cashondeliverytransaction',
            name='amount',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='cashondeliverytransaction',
            name='confirmed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='cashondeliverytransaction',
            name='date_confirmed',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='cashondeliverytransaction',
            name='reference',
            field=models.CharField(blank=True, default=payment.models._make_uuid, max_length=100, unique=True),
        ),
    ]

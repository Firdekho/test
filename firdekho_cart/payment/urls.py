from django.urls import path
from .views import CodView

app_name = "payment"
urlpatterns = [
    path('cod/', CodView.as_view(), name="cod")
]
